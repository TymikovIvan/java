package com.company;

import java.math.BigDecimal;

public class Car {
    int id;
    String brand, model, year, color, number, price;

    Car(int id, String brand, String model, String year, String color, String price, String number) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.year = year;
        this.color = color;
        this.price = price;
        this.number = number;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getId() {
        return id;
    }
    public String getBrand() {
        return brand;
    }
    public String getModel() {
        return model;
    }
    public String getYear() {
        return year;
    }
    public String getColor() {
        return color;
    }
    public String getPrice() {
        return price;
    }
    public String getNumber() {
        return number;
    }

    public String toString() {
        return "ID:"+id+" Марка: "+brand+" Модель: "+model+" Год выпуска: "+year+" Цвет: "+color+" Цена: "+price+" рублей Рег. номер:"+number;
    }
}
