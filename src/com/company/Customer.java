package com.company;

public class Customer implements Comparable<Customer> {
    int id;
    String surname, name, patronymic, address, numberBank;
    long numberCard;

    Customer(int id, String surname, String name, String patronymic, String address, long numberCard, String numberBank) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.address = address;
        this.numberCard = numberCard;
        this.numberBank = numberBank;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setNumberCard(long numberCard) {
        this.numberCard = numberCard;
    }

    public void setNumberBank(String numberBank) {
        this.numberBank = numberBank;
    }

    public int getId() {
        return id;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getAddress() {
        return address;
    }

    public long getNumberCard() {
        return numberCard;
    }

    public String getNumberBank() {
        return numberBank;
    }

    public String toString() {
        return "ФИО: "+surname+" "+name+" "+patronymic+"\n"+"Адрес: "+address+"\n"+"Номер кредитной карты: "+numberCard+"\n"+"Номер банковского счета: "+numberBank;
    }

    @Override
    public int compareTo(Customer o) {
        return this.name.compareTo(o.name);
    }
}
