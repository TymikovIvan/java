package com.company;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	    int arrT1[] = {10,8,9,4,7,3,5,2,1,6};
        int arrT2[] = {15,10,12,31,23,41,34};
        Scanner scan = new Scanner(System.in);
        for(int i = arrT1.length-1 ; i > 0 ; i--){
            for(int j = 0 ; j < i ; j++){
                if( arrT1[j] > arrT1[j+1] ){
                    int max = arrT1[j];
                    arrT1[j] = arrT1[j+1];
                    arrT1[j+1] = max;
                }
            }
        }
        System.out.println("Блок 1");
        System.out.println("Задание №1");
        System.out.print("По возрастанию:");
        for (int i = 0; i < arrT1.length; i++) {
            if (i == arrT1.length-1) {
                System.out.println(arrT1[i]+".");
            } else {
                System.out.print(arrT1[i]+", ");
            }
        }

        for(int i = arrT1.length-1 ; i > 0 ; i--){
            for(int j = 0 ; j < i ; j++){
                if( arrT1[j] < arrT1[j+1] ){
                    int min = arrT1[j];
                    arrT1[j] = arrT1[j+1];
                    arrT1[j+1] = min;
                }
            }
        }
        System.out.print("По убыванию:");
        for (int i = 0; i < arrT1.length; i++) {
            if (i == arrT1.length-1) {
                System.out.println(arrT1[i]+".");
            } else {
                System.out.print(arrT1[i]+", ");
            }
        }

        System.out.println("Задание №2");
        System.out.print("Исходный массив:");
        for (int i = 0; i < arrT2.length;i++) {
            if (i == arrT2.length-1) {
                System.out.println(arrT2[i]+".");
            } else {
                System.out.print(arrT2[i]+", ");
            }
        }
        for (int i = 0; i <arrT2.length;i++) {
            String str = (arrT2[i]+"");
            char ch[] = str.toCharArray();
            for (int j = 0; j < ch.length-1; j++) {
                if (ch[j] < ch[j+1]) {
                    int a = ch[j];
                    int b = ch[j+1];
                    if (b - a == 1) {
                        System.out.print(arrT2[i]+" ");
                    }
                }
            }
        }
        System.out.println();
        System.out.println("Задание №3");
        String monthS;
        System.out.println("Введите номер месяца:");
        int month = scan.nextInt();
        switch (month) {
            case 1: monthS = "Январь";
            break;
            case 2: monthS = "Февраль";
            break;
            case 3: monthS = "Март";
            break;
            case 4: monthS = "Апрель";
            break;
            case 5: monthS = "Май";
            break;
            case 6: monthS = "Июнь";
            break;
            case 7: monthS = "Июль";
            break;
            case 8: monthS = "Август";
            break;
            case 9: monthS = "Сентябрь";
            break;
            case 10: monthS = "Октябрь";
            break;
            case 11: monthS = "Ноябрь";
            break;
            case 12: monthS = "Декабрь";
            break;
            default: monthS = "Такого месяца нет";
        }
        System.out.println(monthS);

        System.out.println("Задание №4");
        int matrix [][] = new int[4][4];
        int matrixX[] = new int[16];
        Random random = new Random();
        int r = 10;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                matrix[i][j] = random.nextInt(r)+ 1;
            }
        }
        System.out.println("Исходная матрица:");
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print(matrix[i][j]+" ");
            }
            System.out.println();
        }
        int a = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                matrixX[a] = matrix[i][j];
                a++;
            }
        }

        for(int i = matrixX.length-1; i > 0 ; i--){
            for(int j = 0 ; j < i ; j++){
                if( matrixX[j] > matrixX[j+1] ){
                    int max = matrixX[j];
                    matrixX[j] = matrixX[j+1];
                    matrixX[j+1] = max;
                }
            }
        }

        a = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                matrix[i][j] = matrixX[a];
                a++;
            }
        }

        System.out.println("Сортировка по возрастанию:");
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print(matrix[i][j]+" ");
            }
            System.out.println();
        }

        for(int i = matrixX.length-1; i > 0 ; i--){
            for(int j = 0 ; j < i ; j++){
                if( matrixX[j] < matrixX[j+1] ){
                    int min = matrixX[j];
                    matrixX[j] = matrixX[j+1];
                    matrixX[j+1] = min;
                }
            }
        }

        a = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                matrix[i][j] = matrixX[a];
                a++;
            }
        }

        System.out.println("Сортировка по убыванию:");
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                System.out.print(matrix[i][j]+" ");
            }
            System.out.println();
        }


        System.out.println("Блок 2");
        System.out.println("Задание №1");

        Student[] students = new Student[5];
        students[0] = new Student(1, "Тюмиков", "Иван", "Викторович", "6 июля 1998 года", "г. Оренбург", "8 (971) 680-60-41", "Информационных технологий", 4, "14 ПКС-1");
        students[1] = new Student(2, "Наумова", "Елена", "Григорьевна", "7 июля 1983", "г. Рязань", "8 (913) 683-49-95", "Юридический", 6, "12 ЮРС-3");
        students[2] = new Student(3, "Мамедов", "Георгий", "Аркадьевич", "20 мая 1974 года", "г. Москва", "8 (971) 313-83-69", "Экономический", 2, "16 ЭОП-1");
        students[3] = new Student(4, "Васильев", "Вячеслав", "Семенович", "20 октября 1990 года", "г. Еткуль", "8 (921) 782-53-63", "Информационных технологий", 4, "14 ПКС-1");
        students[4] = new Student(5, "Сморчкова", "Марта", "Степановна", "21 октября 1963 года", "г. Железногорск", "8 (968) 953-84-12", "Экономический", 3, "15 ЭОП-1");

        System.out.println("Выберите факультет:");
        System.out.println("1 - Информационных технологий");
        System.out.println("2 - Юридический");
        System.out.println("3 - Экономический");
        int facul = scan.nextInt();
        switch (facul) {
            case 1: for (Student student : students) {
                if (student.getFaculty().equals("Информационных технологий")) {
                    System.out.println(student.toString());
                }
            }
            break;
            case 2: for (Student student : students) {
                if (student.getFaculty().equals("Юридический")) {
                    System.out.println(student.toString());
                }
            }
            break;
            case 3: for (Student student : students) {
                if (student.getFaculty().equals("Экономический")) {
                    System.out.println(student.toString());
                }
            }
            break;
            default: System.out.println("Такого факультета нет");
        }
        System.out.println("Факультет информационных технологий:");
        for (Student student : students) {
            if (student.getFaculty().equals("Информационных технологий")) {
                System.out.println(student.toString());
            }
        }
        System.out.println("Юридический факультет:");
        for (Student student : students) {
            if (student.getFaculty().equals("Юридический")) {
                System.out.println(student.toString());
            }
        }
        System.out.println("Экономический факультет:");
        for (Student student : students) {
            if (student.getFaculty().equals("Экономический")) {
                System.out.println(student.toString());
            }
        }
        System.out.println("Студенты 2 курса:");
        for (Student student : students) {
            if (student.getCourse()==2) {
                System.out.println(student.toString());
            }
        }
        System.out.println("Студенты 3 курса:");
        for (Student student : students) {
            if (student.getCourse()==3) {
                System.out.println(student.toString());
            }
        }
        System.out.println("Студенты 4 курса:");
        for (Student student : students) {
            if (student.getCourse()==4) {
                System.out.println(student.toString());
            }
        }
        System.out.println("Студенты 6 курса:");
        for (Student student : students) {
            if (student.getCourse()==6) {
                System.out.println(student.toString());
            }
        }

        System.out.println("Выберите учебную группу (14 ПКС-1, 12 ЮРС-3, 16 ЭОП-1, 15 ЭОП-1):");
        String group;
        group = scan.next();

        switch (group.toUpperCase()) {
            case "14 ПКС-1": for (Student student : students) {
                if (student.getGroup().equals("14 ПКС-1")) {
                    System.out.println(student.toString());
                }
            }
            break;
            case "12 ЮРС-3": for (Student student : students) {
                if (student.getGroup().equals("12 ЮРС-3")) {
                    System.out.println(student.toString());
                }
            }
            break;
            case "16 ЭОП-1": for (Student student : students) {
                if (student.getGroup().equals("16 ЭОП-1")) {
                    System.out.println(student.toString());
                }
            }
            break;
            case "15 ЭОП-1": for (Student student : students) {
                if (student.getGroup().equals("15 ЭОП-1")) {
                    System.out.println(student.toString());
                }
            }
            break;
            default: System.out.println("В этой группе нет студентов");
        }

        System.out.println("Задание №2");
        Car cars[] = new Car[5];
        cars[0] = new Car(1,"Mazda","Mazda6","2018","Красный","5000000","E100EE");
        cars[1] = new Car(2,"BMW","BMW M3 SK Plus","2015","Белый","11000000","A130ВС");
        cars[2] = new Car(3,"Mazda","Mazda2 Sport Black","2017","Черный","6500000","A002CA");
        cars[3] = new Car(4,"Mazda","Mazda2 Sedan","2015","Красный","4500000","О555ОО");
        cars[4] = new Car(5,"Kia","Kia K5 Hybrid","2016","Желтый","2500000","T101TE");

        System.out.println("Введите интересующую марку автомобиля (Mazda, BMW, Kia):");
        String brand = scan.next();
        switch (brand.toLowerCase()) {
            case "Mazda": for (Car car : cars) {
                if (car.getBrand().equals("Mazda")) {
                    System.out.println(car.toString());
                }
            }
            break;
            case "BMW": for (Car car : cars) {
                if (car.getBrand().equals("BMW")) {
                    System.out.println(car.toString());
                }
            }
            break;
            case "Kia": for (Car car : cars) {
                if (car.getBrand().equals("Kia")) {
                    System.out.println(car.toString());
                }
            }
            break;
            default: System.out.println("Такой марки в списке нет");
        }

        System.out.print("Выберите интересующую модель (Mazda6, BMW M3 SK Plus, Mazda2 Sport Black, Mazda2 Sedan, Kia K5 Hybrid):");
        String model = scan.next();
        System.out.println("Лет в эксплуатации: ");
        int year = scan.nextInt();
        int yearT = Calendar.getInstance().get(Calendar.YEAR);
        switch (model) {
            case "Mazda6": for (Car car : cars) {
                if (car.getModel().equals("Mazda6"))  {
                    if (yearT - Integer.parseInt(car.getYear()) > year) {
                        System.out.println(car.toString());
                    }
                }
            }
                break;
            case "BMW M3 SK Plus": for (Car car : cars) {
                if (car.getModel().equals("BMW M3 SK Plus"))  {
                    if (yearT - Integer.parseInt(car.getYear()) > year) {
                        System.out.println(car.toString());
                    }
                }
            }
                break;
            case "Mazda2 Sport Black": for (Car car : cars) {
                if (car.getModel().equals("Mazda2 Sport Black"))  {
                    if (yearT - Integer.parseInt(car.getYear()) > year) {
                        System.out.println(car.toString());
                    }
                }
            }
                break;
            case "Mazda2 Sedan": for (Car car : cars) {
                if (car.getModel().equals("Mazda2 Sedan"))  {
                    if (yearT - Integer.parseInt(car.getYear()) > year) {
                        System.out.println(car.toString());
                    }
                }
            }
                break;
            case "Kia K5 Hybrid": for (Car car : cars) {
                if (car.getModel().equals("Kia K5 Hybrid"))  {
                    if (yearT - Integer.parseInt(car.getYear()) > year) {
                        System.out.println(car.toString());
                    }
                }
            }
                break;
            default: System.out.println("Таких машин нет");
            break;
        }
        System.out.println("Введите интересующий год выпуска:");
        year = scan.nextInt();
        System.out.println("Введите минимальную цену:");
        int price = scan.nextInt();
        switch (year) {
            case 2015: for (Car car : cars) {
                if (car.getYear().equals("2015"))  {
                    if (Integer.parseInt(car.getPrice()) > price) {
                        System.out.println(car.toString());
                    }
                }
            }
                break;
            case 2016: for (Car car : cars) {
                if (car.getYear().equals("2016"))  {
                    if (Integer.parseInt(car.getPrice()) > price) {
                        System.out.println(car.toString());
                    }
                }
            }
                break;
            case 2017: for (Car car : cars) {
                if (car.getYear().equals("2017"))  {
                    if (Integer.parseInt(car.getPrice()) > price) {
                        System.out.println(car.toString());
                    }
                }
            }
                break;
            case 2018: for (Car car : cars) {
                if (car.getYear().equals("2018"))  {
                    if (Integer.parseInt(car.getPrice()) > price) {
                        System.out.println(car.toString());
                    }
                }
            }
                break;
            default: System.out.println("Таких машин нет");
        }

        System.out.println("Задание №3");
        Customer customers[] = new Customer[5];
        customers[0] = new Customer(1,"Тюмиков","Иван","Викторович","г. Оренбург",4875412156961425L,"87451.964.4.7135.1469875");
        customers[1] = new Customer(2,"Лапина","Элла","Николаевна","г. Сорочинск",8412751222413645L,"31642.644.1.1474.6954685");
        customers[2] = new Customer(3,"Зеленова","Мира","Андреевна","г. Лянтор",4875412156961425L,"13247.794.8.0312.7018546");
        customers[3] = new Customer(4,"Муравьёва","Фаина","Леонидовна","г. Луховицы",4875412156961425L,"12048.745.1.6547.8547122");
        customers[4] = new Customer(5,"Кузьмина","Ариадна","Константиновна","г. Петропавловское",4875412156961425L,"41021.000.0.0022.0321022");

        Arrays.sort(customers);

        for (Customer customer : customers) {
            System.out.println(customer.toString());
        }
        System.out.println();
        Circle circle[] = new Circle[5];
        circle[0] = new Circle(15);
        circle[1] = new Circle(17);
        circle[2] = new Circle(14);
        circle[3] = new Circle(19);
        circle[4] = new Circle(16);

        double max = circle[0].area();
        for (int i = 0; i < 5; i++) {
            if (circle[i].area() > max) {
                max = circle[i].area();
            }
        }
        System.out.println("Максимальная площадь = "+max);
        double min = circle[0].area();
        for (int i = 0; i < 5; i++) {
            if (circle[i].area() < min) {
                min  = circle[i].area();
            }
        }
        System.out.println("Минимальная площадь = "+min);

        max = circle[0].perimeter();
        for (int i = 0; i < 5; i++) {
            if (circle[i].perimeter() > max) {
                max = circle[i].perimeter();
            }
        }
        System.out.println("Максимальный периметр = "+max);

        min = circle[0].perimeter();
        for (int i = 0; i < 5; i++) {
            if (circle[i].perimeter() < min) {
                min = circle[i].perimeter();
            }
        }
        System.out.println("Минимальный периметр = "+min);
    }
}
