package com.company;

public class Student {
    int id;
    String surname, name, patronymic, born, address, phone, faculty, group;
    int course;

    Student (int id, String surname, String name, String patronymic, String born, String address, String phone, String faculty, int course, String group) {
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.born = born;
        this.address = address;
        this.phone = phone;
        this.faculty = faculty;
        this.course = course;
        this.group = group;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public void setBorn(String born) {
        this.born = born;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public void setCourse(int course) {
        this.course = course;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public int getId() {
        return id;
    }
    public String getSurname() {
        return surname;
    }
    public String getName() {
        return name;
    }
    public String getPatronymic() {
        return patronymic;
    }
    public String getBorn() {
        return born;
    }
    public String getAddress() {
        return address;
    }
    public String getPhone() {
        return phone;
    }
    public String getFaculty() {
        return faculty;
    }
    public int getCourse() {
        return course;
    }
    public String getGroup() {
        return group;
    }

    public String toString() {
        return "ID: "+id+" ФИО: "+surname+" "+name+" "+patronymic+" "+" Дата рождения: "+born+" Адрес: "+address+" Телефон: "+phone+" Факультет: "+faculty+" Курс: "+course+" Группа: "+group;
    }
}
