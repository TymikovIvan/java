package com.company;

public class Circle {
    double r;

    Circle(double r) {
        this.r = r;
    }

    public double area() {
        double ar;
        ar = Math.PI * Math.pow(r,2);
        return ar;
    }

    public double perimeter() {
        double p;
        p = 2 * Math.PI * r;
        return p;
    }
}
